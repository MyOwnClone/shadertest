/**
 *	Copyright (c) 2012 Devon O. Wolfgang
 *
 *	Permission is hereby granted, free of charge, to any person obtaining a copy
 *	of this software and associated documentation files (the "Software"), to deal
 *	in the Software without restriction, including without limitation the rights
 *	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *	copies of the Software, and to permit persons to whom the Software is
 *	furnished to do so, subject to the following conditions:
 *
 *	The above copyright notice and this permission notice shall be included in
 *	all copies or substantial portions of the Software.
 *
 *	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *	AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *	LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *	OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *	THE SOFTWARE.
 */
 
package
{
	import com.adobe.utils.AGALMacroAssembler;
	import flash.display.BitmapData;
	import flash.display.Sprite;
	import flash.display.StageAlign;
	import flash.display.StageScaleMode;
	import flash.display3D.Context3D;
	import flash.display3D.Context3DProgramType;
	import flash.display3D.Context3DTextureFormat;
	import flash.display3D.Context3DVertexBufferFormat;
	import flash.display3D.IndexBuffer3D;
	import flash.display3D.Program3D;
	import flash.display3D.textures.Texture;
	import flash.display3D.VertexBuffer3D;
	import flash.events.Event;
	import flash.geom.Matrix3D;
 
	[SWF(width="512", height="512", frameRate="60", backgroundColor="#000000")]
	public class ShaderExample extends Sprite
	{
		// make sure this image is in power of 2
		[Embed(source="mtns.jpg")]
		protected const TEXTURE:Class;
 
		// shader code
		[Embed(source="shader.macro", mimeType="application/octet-stream")]
		protected const ASM:Class;
 
		private var mContext3d:Context3D;
		private var mVertBuffer:VertexBuffer3D;
		private var mIndexBuffer:IndexBuffer3D; 
		private var mProgram:Program3D;
		private var mTexture:Texture;
		private var mTextureData:BitmapData;
 
		private var mMatrix:Matrix3D = new Matrix3D();
 
		public function ShaderExample()
		{	
			if (stage) init();
			else addEventListener(Event.ADDED_TO_STAGE, init);	
		}
 
		private function init(event:Event = null):void
		{
			removeEventListener(Event.ADDED_TO_STAGE, init);
 
			initStage();
			initTexture();
 
			addEventListener(Event.ENTER_FRAME, onTick);
		}
 
		private function initTexture():void
		{
			mTextureData = new TEXTURE().bitmapData;
 
			stage.stage3Ds[0].addEventListener( Event.CONTEXT3D_CREATE, initStage3d );
			stage.stage3Ds[0].requestContext3D();
		}
 
		private function initStage():void
		{
			stage.scaleMode = StageScaleMode.NO_SCALE;
			stage.align = StageAlign.TOP_LEFT;
		}
 
		private function initStage3d(event:Event):void
		{
			mContext3d = stage.stage3Ds[0].context3D;		
			mContext3d.enableErrorChecking = true;	// set this to false when complete to improve performance
 
			mContext3d.configureBackBuffer(stage.stageWidth, stage.stageHeight, 1, true);
 
			var vertices:Vector.<Number> = Vector.<Number>([
			//	x		y		z			u 	v
				-1.0, 	-1.0, 	0,  		0, 0, 
				-1.0,  	1.0, 	0, 			0, 1,
				 1.0,  	1.0, 	0, 			1, 1,
				 1.0, 	-1.0, 	0,			1, 0  ]);
 
			mVertBuffer = mContext3d.createVertexBuffer(4, 5);
			mVertBuffer.uploadFromVector(vertices, 0, 4);
 
			mIndexBuffer = mContext3d.createIndexBuffer(6);			
			mIndexBuffer.uploadFromVector (Vector.<uint>([0, 1, 2, 2, 3, 0]), 0, 6);
 
			mTexture = mContext3d.createTexture(mTextureData.width, mTextureData.height, Context3DTextureFormat.BGRA, true);
			mTexture.uploadFromBitmapData(mTextureData);
 
			// va0 holds xyz
			mContext3d.setVertexBufferAt(0, mVertBuffer, 0, Context3DVertexBufferFormat.FLOAT_3);
 
			// va1 holds uv
			mContext3d.setVertexBufferAt(1, mVertBuffer, 3, Context3DVertexBufferFormat.FLOAT_2);
 
			generateMacroProg();
 
			mContext3d.setTextureAt(0, mTexture);
			mContext3d.setProgram(mProgram);
		}	
 
		/**
		 * Creates the shader program from the embedded .macro file
		 */
		private function generateMacroProg():void
		{
			var asm:String = String(new ASM());
 
			// split the vertex and fragment shaders
			var codeSplit:Array = asm.split("####");
 
			var macroVertex:AGALMacroAssembler 		= new AGALMacroAssembler();
			var macroFragment:AGALMacroAssembler 	= new AGALMacroAssembler();
 
			macroVertex.assemble(Context3DProgramType.VERTEX, codeSplit[0]);
			macroFragment.assemble(Context3DProgramType.FRAGMENT, codeSplit[1]);
 
		//	trace("VERTEX: \n" + macroVertex.asmCode);
		//	trace("FRAGMENT: \n" + macroFragment.asmCode);
 
			mProgram = mContext3d.createProgram();
			mProgram.upload( macroVertex.agalcode, macroFragment.agalcode);
		}
 
 
		private var mTime:Number = 0.0;
		private function onTick(event:Event):void
		{
			if ( !mContext3d ) 
				return;
 
			mContext3d.clear ( 0, 0, 0, 1 );
 
			// set vertex data from blank Matrix3D
			mContext3d.setProgramConstantsFromMatrix(Context3DProgramType.VERTEX, 0, mMatrix, true);
 
			// Fragment shader constants go here
 
			// resolution
			mContext3d.setProgramConstantsFromVector(Context3DProgramType.FRAGMENT, 3, Vector.<Number>( [ 1, 1, 1, 1 ]) );
 
			// time
			mContext3d.setProgramConstantsFromVector(Context3DProgramType.FRAGMENT, 4, Vector.<Number>([ .20 * mTime, Math.sin(.25 * mTime) , Math.cos(.25 * mTime), 1  ]) );
 
			// ONE (identity)
			mContext3d.setProgramConstantsFromVector(Context3DProgramType.FRAGMENT, 5, Vector.<Number>([ 1, 1, 1, 1 ]) );
 
			// Numbers
			mContext3d.setProgramConstantsFromVector(Context3DProgramType.FRAGMENT, 6, Vector.<Number>([ 2, .250, 1, 1 ]) );
 
			mContext3d.drawTriangles(mIndexBuffer);
			mContext3d.present();
 
			mTime += .025;
		}
	}
}